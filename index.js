const Koa = require('koa')
const app = new Koa()
const bodyParser = require('koa-body')
const mongoose = require('mongoose')

const tasks = require('./routes/tasks')
const users = require('./routes/users')
const songs = require('./routes/songs')
const comments = require('./routes/comments')

app.use(bodyParser())
app.use(tasks.routes())
app.use(users.routes())
app.use(songs.routes())
app.use(comments.routes())

mongoose.connect(
    'mongodb://localhost/koaproba',
    { useNewUrlParser : true }
)
app.listen(5000, ()=> {
    console.log("Listening at port 5000.")
})