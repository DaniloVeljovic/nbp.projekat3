const AccessToken = require("./models/AccessToken");
const User = require("./models/User");
exports.checkAccessToken = async accessToken => {
  return await AccessToken.findOne({
    access_token: accessToken
  }).then(user => {
    console.log(user);
    if (user) {
      return true;
    } else return false;
  });
};
