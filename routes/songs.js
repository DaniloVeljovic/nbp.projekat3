const Router = require("koa-router");
const Song = require("../models/Song");
const validator = require("../authentication");
const mongoose = require("mongoose");
const Comment = require("../models/Comment");
const AccessToken = require("../models/AccessToken");

const router = new Router();

/*
    1. vrati pesmu odredjenog imena
    2. vrati sve pesme
    3. vrati sve pesme odredjenog izvodjaca
    4. dodaje pesmu
    5. modifikuje pesmu

    TODO: Da li treba dodati sve pesme koje je dodao odredjeni korisnik?
    TODO: Dodaj logiku za brisanje pesme i svih komentara.
*/

router.get("/api/song/:name", async ctx => {
  await Song.findOne({
    name: ctx.params.name
  })
    //.populate("username")
    .then(data => {
      if (data != undefined) {
        ctx.status = 200;
        ctx.body = data;
      } else {
        ctx.status = 404;
        ctx.message = "Song not found";
      }
    })
    .catch(err => {
      ctx.body = "err " + err;
    });
});

router.get("/api/Pesma/getbyID/:song_id", async ctx => {
  const song_id = ctx.params.song_id;
  try {
    const FoundSong = await Song.findOne({
      _id: song_id
    });

    ctx.status = 200;
    ctx.body = FoundSong;
  } catch (err) {
    ctx.message = "Not found";
    ctx.body = 404;
  }
});

//
router.post("/api/songs", async ctx => {
  const access_token = ctx.request.headers.access_token;
  console.log(await validator.checkAccessToken(access_token));

  if (await validator.checkAccessToken(access_token)) {
    let song = new Song();
    song._id = new mongoose.Types.ObjectId();
    song.username = ctx.request.body.Username;
    song.name = ctx.request.body.Name;
    song.artist = ctx.request.body.Artist;
    song.chords = ctx.request.body.Chords;

    await song
      .save()
      .then(data => {
        ctx.body = data;
        ctx.status = 200;
        ctx.message = "Song has been added.";
      })
      .catch(err => {
        ctx.body = "error: " + err;
      });
  } else {
    ctx.status = 404;
    ctx.message = "Not possible to add song.";
  }
});

router.get("/api/songs", async ctx => {
  const access_token = ctx.request.body.access_token;

  if (await validator.checkAccessToken(access_token)) {
    await Song.find()
      .then(data => {
        ctx.body = data;
        ctx.status = 200;
        ctx.message = "OK";
      })
      .catch(err => {
        console.log("error: " + err);
      });
  } else {
    ctx.status = 404;
    ctx.message = "Error with access token";
  }
});

router.get("/api/songs/:artist", async ctx => {
  await Song.find({
    artist: ctx.params.artist
  })
    .then(data => {
      if (data != undefined) {
        ctx.body = data;
        ctx.status = 200;
      } else {
        ctx.status = 404;
        ctx.message = "Error with the access token";
      }
    })
    .catch(err => {
      ctx.body = "error:" + err;
    });
});

router.put("/api/songs/:name", async ctx => {
  try {
    const accesstoken = ctx.request.headers.access_token;
    const user = await AccessToken.findOne({
      access_token: accesstoken
    }).populate("fk_user");
    console.log(user);
    const song = await Song.findOne({
      name: ctx.params.name
    }).populate("username");

    console.log(song);
    console.log(ctx.request.body.Chords);
    if (user.fk_user.username == song.username.username) {
      await Song.findOneAndUpdate(
        { name: ctx.params.name },
        { chords: ctx.request.body.Chords }
      )
        .then(data => {
          console.log(data);
          ctx.body = data;
          ctx.status = 200;
          ctx.message = "OK";
        })
        .catch(err => {
          ctx.body = `error: ${err}`;
          ctx.status = 404;
        });
    } else {
      ctx.message = "No authority to update the song.";
      ctx.status = 404;
    }
  } catch (err) {
    ctx.message = "error";
    ctx.status = 404;
  }
});

router.get("/api/artists", async ctx => {
  console.log("usao");
  await Song.find()
    .select("artist")
    .then(artists => {
      console.log(artists);
      ctx.body = artists;
      ctx.message = "OKK";
      ctx.status = 200;
    });
});

//delete pesma
router.delete("/api/songs/:song_id", async ctx => {
  const accesstoken = ctx.request.headers.access_token;
  try {
    const user = await AccessToken.findOne({
      access_token: accesstoken
    }).populate("fk_user");

    const song = await Song.findOne({
      _id: ctx.params.song_id
    }).populate("username");

    if (song.username.username == user.fk_user.username) {
      await Comment.deleteMany({
        id_song: ctx.params.song_id
      }).then(
        await Song.deleteOne({
          _id: ctx.params.song_id
        }).then(() => {
          ctx.message = "Deleted";
          ctx.status = 200;
        })
      );
    } else {
      ctx.message = "No authority to delete the song";
      ctx.status = 404;
    }
  } catch (err) {
    ctx.message = "error";
    ctx.status = 404;
  }
});

module.exports = router;
