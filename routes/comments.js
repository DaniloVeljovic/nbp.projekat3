const Router = require("koa-router");
const Comment = require("../models/Comment");
const Song = require("../models/Song");
const validator = require("../authentication");
/*
    1. Dodaj komentar
    2. Obrisi komentar
    3. Izmeni komentar
    4. Vrati sve komentare za odredjenu pesmu
 */

let router = new Router();

//TODO: Frontend mora da ti posalje i Id_pesme za koju se ostavlja komentar!
router.post("/api/comments", async ctx => {
  const access_token = ctx.request.headers.access_token;

  if (await validator.checkAccessToken(access_token)) {
    let comment = new Comment();
    comment.comment = ctx.request.body.Comment;
    comment.username = ctx.request.body.Username;
    //Proveri kako sa frontenda primas ObjectId.
    comment.id_song = ctx.request.body.Id_song;
    comment.index = +new Date();

    console.log(comment);

    await comment
      .save()
      .then(data => {
        ctx.body = data;
        ctx.status = 200;
      })
      .catch(err => {
        console.log(err + "ovde sam");
        ctx.body = `error: ${err}`;
      });
  } else {
    ctx.status = 404;
    ctx.message = "Error with the access token.";
  }
});

router.delete("/api/comments/:id", async ctx => {
  const access_token = ctx.request.headers.access_token;

  if (await validator.checkAccessToken(access_token)) {
    await Comment.deleteOne({
      _id: ctx.params.id
    })
      .then(() => {
        ctx.status = 200;
        ctx.message = "Comment deleted.";
      })
      .catch(err => {
        ctx.body = `Error: ${err}`;
      });
  } else {
    ctx.status = 404;
    ctx.message = "Error with the access token.";
  }
});

//Zahtevalo bi dodatne izmene! Ne bih radio to.
router.put("/api/comments/:id", async ctx => {
  const access_token = ctx.request.headers.access_token;
  if (access_token && (await validator.checkAccessToken(access_token))) {
    await Comment.findOneAndUpdate({
      _id: ctx.params.id
    })
      .then(data => {
        ctx.body = data;
        ctx.status = 200;
      })
      .catch(err => {
        ctx.body = `error: ${err}`;
      });
  } else {
    ctx.status = 404;
    ctx.message = "Error with the access token.";
  }
});

router.get("/api/comments/:song_id", async ctx => {
  await Comment.find({
    id_song: ctx.params.song_id
  })
    //.populate("username")
    //.populate("id_song")
    .then(data => {
      ctx.body = data;
      ctx.status = 200;
    })
    .catch(err => {
      ctx.body = `error: ${err}`;
    });
});

module.exports = router;
