const Router = require("koa-router");
const router = new Router();
const User = require("../models/User");
const validator = require("../authentication");
const AccessToken = require("../models/AccessToken");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");

router.get("/api/user", async ctx => {
  await User.findOne({
    username: ctx.request.headers.username
  }).then(data => {
    if (data != undefined) {
      ctx.body = data;
      ctx.status = 200;
    } else {
      ctx.body = JSON.parse("{}");
    }
  });
});

//vraca jednog korisnika
router.get("/api/user/:id", async ctx => {
  await User.findOne({
    _id: ctx.params.id
  })
    .then(user => {
      if (user == undefined) {
        ctx.status = 404;
        ctx.message = "User not found.";
      } else {
        ctx.body = user;
        ctx.status = 200;
      }
    })
    .catch(err => {
      ctx.status = 404;
      ctx.message = err;
    });
});
//dodaje korisnika
router.post("/api/user", async ctx => {
  if (!ctx.request.body.Username) {
    ctx.body = {
      error: "Bad data"
    };
  } else {
    let user = new User();
    user._id = new mongoose.Types.ObjectId();
    user.username = ctx.request.body.Username;

    const hashedPassword = await bcrypt.hash(ctx.request.body.Password, 10);
    user.password = hashedPassword;
    console.log("Hashed password: " + hashedPassword);
    console.log("user pass: " + user.password);

    user.ime = ctx.request.body.Ime;
    user.prezime = ctx.request.body.Prezime;

    await user.save().then(async userSaved => {
      let accessToken = new AccessToken();
      accessToken.fk_user = user._id;
      accessToken.access_token = +new Date();
      await accessToken
        .save()
        .then(data => {
          ctx.status = 200;
          ctx.body = data;
          ctx.message = "Kreiran korisnik";
        })
        .catch(err => {
          ctx.body = "error: " + err;
        });
    });
  }
});

//access token se salje kao parametar zahteva
router.get("/api/accesstoken/:at", async ctx => {
  const accessToken = ctx.params.at;
  await AccessToken.findOne({
    access_token: accessToken
  })
    .populate("fk_user")
    .then(token => {
      if (token != undefined) {
        console.log(token.fk_user.username);
        ctx.status = 200;
        //moze da se vrati ceo objekat a i samo ime kako god
        ctx.body = JSON.parse(`{"username": "${token.fk_user.username}"}`);
      } else {
        ctx.message = "No user with this access token";
        ctx.status = 404;
      }
    });
});

//vraca access token objekat
router.get("/api/login", async ctx => {
  const sent_username = ctx.request.headers.username;
  const sent_password = ctx.request.headers.password;

  try {
    let user = await User.findOne({
      username: sent_username
    });

    const match = await bcrypt.compare(sent_password, user.password);
    if (match) {
      let access_token = await AccessToken.findOne({
        fk_user: user._id
      });

      console.log(access_token);
      ctx.body = access_token;
      ctx.status = 200;
    } else {
      ctx.status = 404;
      ctx.message = "No account.";
    }
  } catch (err) {
    ctx.status = 404;
    ctx.message = "Not found";
  }
});

module.exports = router;
