﻿const btn = document.getElementById("prijava-btn");

const clrLightBtn = "#d4d2cb";
const clrDarkBtn = "#8f8494";

btn.onmouseover = (ev) => {
    btn.style.backgroundColor = clrDarkBtn;
    btn.style.border = "2px solid black";
}
btn.onmouseout = (ev) => {
    btn.style.backgroundColor = clrLightBtn;
    btn.style.border = "1px solid black";
}

const errPrijava = document.getElementById("neuspesnaPrijavaTxt");
const usernamePrijava = document.getElementById("prijava-username");
const passPrijava = document.getElementById("prijava-password");

usernamePrijava.onclick = (ev) =>
{
    hideErrPrijava();
}
passPrijava.onclick = (ev) =>
{
    hideErrPrijava();
}

function hideErrPrijava()
{
    if (errPrijava != null)
    {
        errPrijava.style.display = "none";
    }
}