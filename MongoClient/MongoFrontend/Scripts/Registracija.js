﻿const btn = document.getElementById("registracija-btn");

const clrLightBtn = "#a797ad";
const clrDarkBtn = "#8f8494";

btn.onmouseover = (ev) =>
{
    btn.style.backgroundColor = clrDarkBtn;
    btn.style.border = "2px solid black";
}
btn.onmouseout = (ev) =>
{
    btn.style.backgroundColor = clrLightBtn;
    btn.style.border = "1px solid black";
}

const regUsername = document.getElementById("registracija-username");

const regErr = document.getElementById("registracija-greska-text");

regUsername.onclick = (ev) =>
{
    HideErrText();
}


function HideErrText()
{
    if (regErr != null)
    {
        regErr.style.display = "none";
    }
}
