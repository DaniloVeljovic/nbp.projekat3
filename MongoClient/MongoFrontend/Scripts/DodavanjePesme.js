﻿const btn = document.getElementById("dodavanje-pesme-btn");

const clrLightBtn = "#789fde";
const clrDarkBtn = "#55719e";

btn.onmouseover = (ev) =>
{
    btn.style.backgroundColor = clrDarkBtn;
    btn.style.border = "2px solid black";
}
btn.onmouseout = (ev) =>
{
    btn.style.backgroundColor = clrLightBtn;
    btn.style.border = "1px solid black";
}

