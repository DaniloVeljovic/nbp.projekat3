﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoFrontend.Models;
using System.Net.Http;

namespace MongoFrontend.Controllers
{
    public class BendoviController : Controller
    {
        // GET: Bendovi
        public async System.Threading.Tasks.Task<ActionResult> Index(String id)
        {
            if (id==null)
            {
                return RedirectToAction("Index", "Home");
            }

            HttpClient client = HttpClientRepository.Client;
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/artists");
            List<Bend> bendovi = null;
            List<string> listaBendova = new List<string>();

            if (!response.IsSuccessStatusCode)
            {
               return RedirectToAction("Index", "Home");
            }
            bendovi = await response.Content.ReadAsAsync<List<Bend>>();
            if(bendovi==null)
            {
                return RedirectToAction("Index", "Home");
            }

            foreach (Bend b in bendovi)
            {
                listaBendova.Add(b.Artist);
            }

            listaBendova = listaBendova.Distinct().ToList();
            id=id.ToLower();
            List<string> novaLista = new List<string>();
            if (listaBendova != null)
            {
                foreach (string bend in listaBendova)
                {
                    if (id[0] == bend.ToLower()[0])
                    {
                        novaLista.Add(bend);
                    }
                }
            }

            ViewBag.Bendovi = novaLista;
            ViewBag.Slovo = id.ToUpper();
            
            return View();
        }
        public async System.Threading.Tasks.Task<ActionResult> BendPesme(String id)
        {
            // INDEX JE IME BENDA
            // TREBA DA SE IZ BAE POVUKU SVE PESME ZA IME BENDA I DA SE VRATE VIEW-u KROZ VIEWBAG

            if (id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            HttpClient client = HttpClientRepository.Client;
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/songs/"+id);
            List<Pesma> pesme = null;
            List<string> listaPesama= new List<string>();

            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }
            pesme = await response.Content.ReadAsAsync<List<Pesma>>();
            if (pesme != null)
            {
                foreach (Pesma p in pesme)
                {
                    listaPesama.Add(p.Name);
                }
            }

            ViewBag.Bend = id;
            ViewBag.Pesme = listaPesama;
            return View();
        }
    }
}