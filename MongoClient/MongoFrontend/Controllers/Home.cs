﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using MongoFrontend.Models;

namespace MongoFrontend.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        public async System.Threading.Tasks.Task<ActionResult> DodajPesmu()
        {
            Pesma pesma = new Pesma();
            pesma.Artist = "The Cure";
            pesma.Chords = "sasadsda/nasdasdas/nasdasdas/n";
            pesma.Name = "A strange day";
            pesma.Username = "danan";
            HttpClient client = HttpClientRepository.Client;
            HttpResponseMessage response = await client.PostAsJsonAsync(HttpClientRepository.Uri+"api/songs",pesma);
            return View("Index");
        }
    }
}