﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoFrontend.Models;
using System.Net.Http;

namespace MongoFrontend.Controllers
{
    public class RegistracijaController : Controller
    {
        // GET: Registracija
        public ActionResult Index()
        {
            if (Session["at"] != null)
            {
                return View("VecSteRegistrovani");
            }
            return View();
        }
        [HttpPost]
        public async System.Threading.Tasks.Task<ActionResult> CreateKorisnik(Korisnik korisnik)
        {
            ///OVDE TREBA PROVERA DA LI JE USERNAME ZAUZET,
            ///AKO JESTE ONDA SE VRACA FORMA ZA REGISTRACIJU, A U VIEW BAG SE STAVLJA:
            ///NeuspesnaRegistracija=1    i    Ime=korisnik.Ime     i    Prezime=korisnik.Prezine
            ///AKO NIJE ZAUZET ONDA SE KORISNIK REGISTRUJE i RADI SE
            ///RedirectTo HomeIndexView

            if(korisnik==null)
            {
                return RedirectToAction("Index", "Home");
            }
            HttpClient client = HttpClientRepository.Client;
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("username", korisnik.Username);
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/user");
            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }

            Korisnik k = await response.Content.ReadAsAsync<Korisnik>();


            if(k!=null)
            {
                if (k.Username != null)
                {
                    //korisnik postoji u bazi
                    ViewBag.NeuspesnaRegistracija = 1;
                    ViewBag.Ime = korisnik.Ime;
                    ViewBag.Prezime = korisnik.Prezime;
                    return View("Index");
                }
            }

            //korisnik sa izabranim username ne postoji u bazi, moze da se doda

            response = await client.PostAsJsonAsync(HttpClientRepository.Uri + "api/user",korisnik);
            if(!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }

            AccessToken at = await response.Content.ReadAsAsync<AccessToken>();

            Session["username"] = korisnik.Username;
            Session["at"] = at.Access_token;

            return View("UspesnaRegistracija");
        }
    }
}