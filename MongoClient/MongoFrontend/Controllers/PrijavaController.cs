﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoFrontend.Models;
using System.Net.Http;
using System.Web.Security;

namespace MongoFrontend.Controllers
{
    public class PrijavaController : Controller
    {
        public ActionResult Index()
        {
            if (Session["at"] != null)
            {
                return View("VecStePrijavljeni");
            }
            return View();
        }
        public async System.Threading.Tasks.Task<ActionResult> PrijaviKorisnika(Korisnik korisnik)
        {
            //// OVDE TREBA DA SE PROVERI DA LI U BAZI POSTOJI KORISNIK SA DATIM USERNAME I PASS
            /// AKO POSTOJI, KORISNIK SE PRIJAVLJUJE
            /// AKO NE POSTOJI VRACA SE STRANICA ZA PRIJAVU A U ViewBag SE STAVLJA
            /// NeuspesnaPrijava=1;


            HttpClient client = HttpClientRepository.Client;
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("username", korisnik.Username);
            client.DefaultRequestHeaders.Add("password", korisnik.Password);
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/login");
            if(response.ReasonPhrase.Equals("No account."))
            {
                ViewBag.NeuspesnaPrijava = 1;
                return View("Index");
            }
            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }
            AccessToken at =await response.Content.ReadAsAsync<AccessToken>();
            Session["Username"] = korisnik.Username;
            Session["at"] = at.Access_token;

            return View("UspesnaPrijava");
        }
        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Abandon();
            return RedirectToAction("Index", "Home");
        }
    }
}