﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoFrontend.Models;
using System.Net.Http;

namespace MongoFrontend.Controllers
{
    public class PesmaController : Controller
    {
        // GET: Pesma
        public async System.Threading.Tasks.Task<ActionResult> Index(String id)
        {
            // ovde treba da se u bazi nadje pesma sa imenom id
            // onda da se kreira Model.Pesma sa svim atributima iz baze, i da se ubaci u ViewBag
            // onda se vrati View
            if(id == null)
            {
                return RedirectToAction("Index", "Home");
            }

            HttpClient client = HttpClientRepository.Client;
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/song/" + id);
            Pesma pesmica = null;

            if (!response.IsSuccessStatusCode)
            { 
                return RedirectToAction("Index", "Home");
            }

            pesmica = await response.Content.ReadAsAsync<Pesma>();

            if(pesmica==null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Pesma = pesmica;

            Korisnik korisnik = null;
            response = await client.GetAsync(HttpClientRepository.Uri + "api/user/" + pesmica.Username);
            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }
            korisnik = await response.Content.ReadAsAsync<Korisnik>();
            if(korisnik==null)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.KorisnikUsername = korisnik.Username;

            response = await client.GetAsync(HttpClientRepository.Uri + "api/comments/" + pesmica._id);
            List<Komentar> komentari=null;


            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }

            komentari = await response.Content.ReadAsAsync<List<Komentar>>();
            komentari= komentari.OrderBy(o => o.Index).ToList();

            List<List<string>> komIusers = new List<List<string>>();
            foreach(Komentar kom in komentari)
            {
                Korisnik kor = null;
                response = await client.GetAsync(HttpClientRepository.Uri + "api/user/" + kom.Username);
                if(!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Home");
                }
                kor = await response.Content.ReadAsAsync<Korisnik>();
                if(kor==null)
                {
                    return RedirectToAction("Index", "Home");
                }
                List<string> pom = new List<string>();
                pom.Add(kor.Username);
                pom.Add(kom.Comment);
                komIusers.Add(pom);
            }

            ViewBag.Komentari = komIusers;

            return View();
        }
        public ActionResult DodavanjePesme()
        {
            return View();
        }
        public async System.Threading.Tasks.Task<ActionResult> DodajPesmu(Pesma pesma)
        {
            /// OVDE TREBA DA SE PROVERI DA LI PESMA SA TIM IMENOM VEC POSTOJI U BAZI
            /// AKO VEC POSTOJI VRACA SE VIEW: PesmaVecDodata
            /// AKO NE POSTOJI DODA SE PESMA I RedirectToAction(Index,Home);
            if(Session["username"]==null || Session["at"]==null)
            {
                return View("NeuspesnoDodavanjePesme");
            }

            HttpClient client = HttpClientRepository.Client;

            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/song/" + pesma.Name);

            if (response.ReasonPhrase.Equals("Song not found"))
            {
                // pesma sa zadatim imenom ne postoji u bazi, moze da se doda
                /// OVDE MORA DA SE IDENTIFIKUJE  KORISNIK I DA SE DODA U pesma.Username

                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("username", Session["username"].ToString());
                response = await client.GetAsync(HttpClientRepository.Uri + "api/user");
                if (!response.IsSuccessStatusCode)
                {
                    return View("NeuspesnoDodavanjePesme");
                }

                Korisnik k = await response.Content.ReadAsAsync<Korisnik>();
                if(k==null)
                {
                    return View("NeuspesnoDodavanjePesme");
                }
                if(k.Username==null)
                {
                    return View("NeuspesnoDodavanjePesme");
                }
                pesma.Username = k._id;
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("access_token", Session["at"].ToString());
                response = await client.PostAsJsonAsync(HttpClientRepository.Uri + "api/songs", pesma);
                client.DefaultRequestHeaders.Clear();
                if (!response.IsSuccessStatusCode)
                {
                    return View("NeuspesnoDodavanjePesme");
                }
                return RedirectToAction("Index","Pesma", new { id = pesma.Name });
            }
            else
            {
                if (!response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index", "Home");
                }
                return View("PesmaVecDodata");

            }
        }

        public async System.Threading.Tasks.Task<ActionResult> DodavanjeKomentara(String id)
        {
            HttpClient client = HttpClientRepository.Client;
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/song/" + id);
            Pesma pesma = null;

            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }
            pesma =await response.Content.ReadAsAsync<Pesma>();
            if (pesma == null)
            {
                return RedirectToAction("Index", "Home");
            }

            ViewBag.Pesma = pesma;
            return View();
        }

        public async System.Threading.Tasks.Task<ActionResult> DodajKomentar(Komentar komentar)
        {
            if(Session["username"]==null||Session["at"]==null)
            {
                return RedirectToAction("Index", "Prijava");
            }

            HttpClient client = HttpClientRepository.Client;
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("username", Session["username"].ToString());
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/user");
            Korisnik kor= null;

            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }
            kor = await response.Content.ReadAsAsync<Korisnik>();

            if (kor==null)
            {
                return RedirectToAction("Index", "Home");
            }

            komentar.Username = kor._id;

            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("access_token", Session["at"].ToString());
            response = await client.PostAsJsonAsync(HttpClientRepository.Uri + "api/comments",komentar);
            client.DefaultRequestHeaders.Clear();
            if (!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }

            /// PROVERI OVU METODU
            /// 
            ///DALJE: TREBA DA SE IZ BAZE IZVADI PESMA SA ID-JEM PESME KOJI JE U KOMENTARU
            ///DA SE NAPISE MEDOTA KOJZ IMA PARAMETAR PESMA I PROSLEDJUJE TU PESMU VIEW-U ZA PRIKAZ PESME
            ///

            response = await client.GetAsync(HttpClientRepository.Uri + "api/Pesma/getbyID/" + komentar.Id_song);
            if(!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }
            Pesma pesmica = null;
            pesmica = await response.Content.ReadAsAsync<Pesma>();

            if(pesmica==null)
            {
                return RedirectToAction("Index", "Home");
            }

            return RedirectToAction("Index",new { id=pesmica.Name});
        }

        public async System.Threading.Tasks.Task<ActionResult> BrisanjePesme(string id)
        {
            HttpClient client = HttpClientRepository.Client;
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("access_token", Session["at"].ToString());
            HttpResponseMessage response = await client.DeleteAsync(HttpClientRepository.Uri + "api/songs/"+id);
            client.DefaultRequestHeaders.Clear();
            if (!response.IsSuccessStatusCode)
            {
                ViewBag.Uspesno = false;
            }
            if (response.ReasonPhrase.Equals("No authority to delete the song"))
            {
                ViewBag.Uspesno = false;
            }
            else
            {
                ViewBag.Uspesno=true;
            }
            return View("BrisanjePesmeRezultat");
        }

        public async System.Threading.Tasks.Task<ActionResult> IzmenaPesme(String id)
        {
            //id je ime pesme
            if(Session["username"]==null || Session["at"]==null)
            {
                return RedirectToAction("Index", "Home");
            }

            HttpClient client = HttpClientRepository.Client;
            HttpResponseMessage response = await client.GetAsync(HttpClientRepository.Uri + "api/song/" + id);
            if(!response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index", "Home");
            }
            Pesma pesma = null;

            pesma = await response.Content.ReadAsAsync<Pesma>();

            if(pesma==null)
            {
                return RedirectToAction("Index", "Home");
            }
            ViewBag.Pesma = pesma;
            return View();
        }
        public async System.Threading.Tasks.Task<ActionResult> IzmeniPesmu(Pesma pesma)
        {
            if(Session["username"]==null||Session["at"]==null)
            {
                return RedirectToAction("Index", "Prijava");
            }
            HttpClient client = HttpClientRepository.Client;
            client.DefaultRequestHeaders.Clear();
            client.DefaultRequestHeaders.Add("access_token", Session["at"].ToString());
            HttpResponseMessage response =await client.PutAsJsonAsync(HttpClientRepository.Uri + "api/songs/" + pesma.Name, pesma);
            client.DefaultRequestHeaders.Clear();

            if (!response.IsSuccessStatusCode)
            {
                return View("NeuspesnaIzmena");
            }
            return RedirectToAction("Index", new { id = pesma.Name });

        }
    }
}