﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoFrontend.Models
{
    public class Korisnik
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Ime { get; set; }
        public string Prezime { get; set; }
        public string _id { get; set; }

        public Korisnik() { }
        public Korisnik(string un,string pass,string i,string pr)
        {
            Username = un;
            Password = pass;
            Ime = i;
            Prezime = pr;
        }
    }
}