﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoFrontend.Models
{
    public class AccessToken
    {
     //   public string _id { get; set; }
        public string Access_token { get; set; }
        public string Fk_user { get; set; } // ID korisnika ciji je accesstoken
        public AccessToken() { }
    }
}