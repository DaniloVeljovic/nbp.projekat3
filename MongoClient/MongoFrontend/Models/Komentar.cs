﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoFrontend.Models
{
    public class Komentar
    {
        public string Id_song { get; set; }
        public string Comment { get; set; }
        public string Username { get; set; } // ustvari id korisnika koji je ostavio komentar
        public decimal Index { get; set; }
        public Komentar() { }
    }
}