﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MongoFrontend.Models
{
    public class Pesma
    {
        public string Username { get; set; } // id korisnika koji je ostavio komentar
        public string Chords { get; set; }
        public string Artist { get; set; }
        public string Name { get; set; }
        public string _id { get; set; }
        public Pesma() { }
        public Pesma(string u,string a, string i, string n)
        {
            Username = u;
            Chords = a;
            Artist = i;
            Name = n;
        }
    }
}