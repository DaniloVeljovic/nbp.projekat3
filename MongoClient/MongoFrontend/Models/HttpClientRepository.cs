﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;

namespace MongoFrontend.Models
{
    public class HttpClientRepository
    {
        private static object locker=true;
        private static HttpClient client;
        public static string Uri = "http://localhost:5000/";

        private HttpClientRepository()
        {
            client = new HttpClient();
        }
        public static HttpClient Client
        {
            get
            {
                lock (locker)
                {
                    if (client == null)
                        client = new HttpClient();
                }
                return client;
            }
        }
    }
}