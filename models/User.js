const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId
  },
  username: {
    type: String
  },
  password: {
    type: String
  },
  ime: {
    type: String
  },
  prezime: {
    type: String
  }
});

module.exports = mongoose.model("users", UserSchema);
