const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CommentSchema = new Schema({
  comment: {
    type: String
  },
  username: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  id_song: {
    type: Schema.Types.ObjectId,
    ref: "songs"
  },
  index: {
    type: Number
  }
});

module.exports = mongoose.model("comments", CommentSchema);
