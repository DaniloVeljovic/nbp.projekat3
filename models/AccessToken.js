const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const AccessTokenSchema = new Schema({
  fk_user: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  access_token: {
    type: Number
  }
});

module.exports = mongoose.model("accessTokens", AccessTokenSchema);
