//uzeta konvencija, atributi se pisu sa _
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SongSchema = new Schema({
  _id: {
    type: Schema.Types.ObjectId
  },
  username: {
    type: Schema.Types.ObjectId,
    ref: "users"
  },
  chords: {
    type: String
  },
  artist: {
    type: String
  },
  name: {
    type: String
  }
});

module.exports = mongoose.model("songs", SongSchema);
