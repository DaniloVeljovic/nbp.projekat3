const mongoose = require('mongoose')
const Schema = mongoose.Schema

const GlobalIndexSchema = new Schema({
    global_index:{
        type:Number
    },
    id_song:{
        type:String
    }
})

module.exports = Task = mongoose.model('globalIndex', GlobalIndexSchema)